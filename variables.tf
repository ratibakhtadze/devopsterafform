variable "region" {
        type = string
        description = "This is region variant"
        default = "eu-central-1"
}

variable "vpcname" {
        type = string
        description = "VPC name"
        default = "DevOps Course VPC"
}

variable "vpc_cidr_block" {
        type = string
        description = "VPC IP Range"
        default = "10.0.0.0/16"
}

variable "subnets" {
        type = list(string)
        description = "Availability Zons for VPC"
        default = ["10.0.10.0/24", "10.0.20.0/24" , "10.0.30.0/24"  ]
}

variable "azs" {
        type = list(string)
        description = "Availability Zons for VPC"
        default = ["eu-central-1a", "eu-central-1b" , "eu-central-1c"  ]
}


variable "azs_subnets" {
        type = map
        description = "Availability zone and corresponding subnets"
        default = {
                "eu-central-1a" = "10.0.10.0/24"
                "eu-central-1b" = "10.0.20.0/24"
                "eu-central-1c" = "10.0.30.0/24"
        }
  
}


variable "shhport"{
        type = number
        default = 22
  
}

variable "ingress_tules" {
        type = list(number)
        default = [ 22, 25, 89, 443 ]
  
}

variable "egress_rules" {
        type = list(number)
        default = [ 22, 25, 89, 443 ]
  
}

variable "instance_name" {
        description = "Name of the instance to be created"
        type = string
        default = "devops-training-demo"
}

variable "instance_type" {
        type = string
        default = "t2.micro"
}

variable "subnet_id" {
        description = "The VPC subnet the instance(s) will be created in"
        type = string
        default = "subnet-0e39a87ed4f9cc23e"
}

variable "ami_id" {
        description = "The AMI to use"
        type = string
        default = "ami-0ab1a82de7ca5889c" #"ami-4bd03b24"
}


variable "number_of_instances" {
        description = "number of instances to be created"
        type = number
        default = 1
}

variable "aws_vpc_id" {
        description = "VPC id"
        type = string
        default = "vpc-01dcda5b5e083ba02"
  
}

variable "key_name" {
  default = "SSHGITLAB"
  description = "Desired name of AWS key pair"
}

variable "sshpub" {}
# variable "public_key" {
#   default = var.sshpub       
# }