provider "aws" {
    region = "eu-central-1"
}



resource "aws_key_pair" "ssh_key" {
  key_name   = "${var.key_name}"
  public_key =  "${var.sshpub}"
}

resource "aws_vpc" "myvpc" {
  cidr_block = var.vpc_cidr_block

  tags = {
    Name = var.vpcname
  }
  
}

resource "aws_subnet" "private_subnets" {
  count = length(var.azs)
  vpc_id = aws_vpc.myvpc.id
  cidr_block = element(var.subnets, count.index)
  availability_zone = element(var.azs, count.index)

  tags = {
    Nmae = "Privarte Subnet $count.intext + 1}"
  }
  
}

resource "aws_instance" "ec2_instance" {
    ami = "${var.ami_id}"
    count = "${var.number_of_instances}"
    subnet_id = "${var.subnet_id}"
    instance_type = "${var.instance_type}"
    key_name = aws_key_pair.ssh_key.key_name

}


# connection {
#     # Host name
#     host = self.public_ip
#     # The default username for our AMI
#     user = "ec2-user"
#     # Private key for connection
#     private_key = "${file(var.private_key)}"
#     # Type of connection
#     type = "ssh"
#   }
#     # key_name = "ssh_key"